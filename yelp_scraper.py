#!/bin/env python2.7
# -*- coding: utf-8 -*-

# Yelp Scraper: copies data from Yelp; results used in Jhamat Mahbubani's
# (Yale '14) senior economics thesis looking for a correlation between
# immigrant populations and quality of ethnic cuisine.
# Copyright (C) 2014  David Noetzel

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import argparse
import codecs
import urllib2
import logging
import random
import time
from pyquery import PyQuery

verbosity = 0
american_dates = False
generic_file_names = False
truncated_data = False
no_proxies = False
WAIT_MIN = 30
WAIT_MAX = 60
proxies = []
restaurant_data = 'yelp_restaurant_data.csv'
data_header = 'Name,Address,Unique ID,Closed?,Price,Categories,Delivery,Takeout,Reservations,Alcohol,Ambience,Attire,Waiter Service,Parking,Accepts Credit Card,Noise Level\n'
restaurant_reviews = 'yelp_review_data.csv'
review_header = 'Restaurant Unique ID,Reviewer Unique ID,Reviewer Location,Review Date,Review Stars,Review Text\n'
my_review_header = 'Name,Address,Reviewer Unique ID,Reviewer Location,Review Date,Review Stars,Review Text\n'



def vprint(string, priority):
    """Prints a string in verbose mode. Useful for debugging"""
    if verbosity >= priority:
        timestamp = time.strftime('[%Y/%m/%d %H:%M:%S]', time.localtime())
        f = codecs.open('yelp_scraper.log', 'a', 'utf-8')
        f.write('{0} {1}\n'.format(timestamp, string))
        f.close()

    return



def truncate_data(attributes):
    """Removes the title header from attributes"""
    attributes['delivery'] = attributes['delivery'].replace('Delivery: ', '')
    attributes['takeout'] = attributes['takeout'].replace('Take-out: ', '')
    attributes['reser'] = attributes['reser'].replace('Takes Reservations: ', '')
    attributes['alc'] = attributes['alc'].replace('Alcohol: ', '')
    attributes['ambience'] = attributes['ambience'].replace('Ambience: ', '')
    attributes['attire'] = attributes['attire'].replace('Attire: ', '')
    attributes['waiter'] = attributes['waiter'].replace('Waiter Service: ', '')
    attributes['parking'] = attributes['parking'].replace('Parking: ', '')
    attributes['credit'] = attributes['credit'].replace('Accepts Credit Cards: ', '')
    attributes['noise'] = attributes['noise'].replace('Noise Level: ', '')

    return attributes



def get_random_user_agent():
    user_agents = ['Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.36 Safari/535.7',
                   'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; en-US) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1',
                   'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:8.0) Gecko/20100101 Firefox/8.0']
    selection = random.randint(0, len(user_agents)-1)

    return user_agents[selection]



def get_proxy():
    """Selects a random proxy"""
    if len(proxies) == 0:
        vprint('Error: all proxies dead', 0)
        sys.exit(1)

    selection = random.randint(0, len(proxies)-1)

    return proxies[selection]



def proxify(proxy, url):
    """Puts the specified URL behind a proxy"""
    return proxy+'/browse.php?u='+escape_url(url)+'&b=4'



def deproxify(url):
    """Removes the proxy wrapping around a link"""
    url = url.replace('/browse.php?u=', '')
    url = unescape_url(url)
    url = url.replace('&b=4', '')

    # Proxy domain still in URL
    if url.count('http://') != 1:
        url = url[url.rfind('http://'):]

    return url



def escape_url(url):
    """Replaces real characters with HTML escaped characters"""
    url = url.replace('/', '%2F')
    url = url.replace(':', '%3A')
    url = url.replace('?', '%3F')
    url = url.replace('=', '%3D')
    url = url.replace('&', '%26')

    return url



def unescape_url(url):
    """Replaces HTML escaped characters with real characters"""
    url = url.replace('%2F', '/')
    url = url.replace('%3A', ':')
    url = url.replace('%3F', '?')
    url = url.replace('%3D', '=')
    url = url.replace('%26', '&')

    return url



def get_query(dest_url):
    """Returns a PyQuery object of the URL behind a proxy"""
    waiting = True
    error_count = 0

    if not no_proxies:
        proxy = get_proxy()

    while waiting:
        if no_proxies:
            if error_count >= 5:
                vprint('IP address blocked; please acquire a new IP address and re-run', 0)
                sys.exit(1)
        else:
            if error_count >= 3:
                proxies.remove(proxy)
                proxy = get_proxy()
                error_count = 0

        wait_time = random.randint(WAIT_MIN, WAIT_MAX)
        try:
            if no_proxies:
                req = urllib2.Request(dest_url)
            else:
                proxied_url = proxify(proxy, dest_url)
                req = urllib2.Request(proxy)
                resp = urllib2.urlopen(req)
                req = urllib2.Request(proxied_url)
                req.add_header('Referer', proxy+'/')

            # Select random user agent with each new request
            req.add_header('User-Agent', get_random_user_agent())
            resp = urllib2.urlopen(req)
            p = PyQuery(resp.read())

            # Proxy prohibits hotlinking
            if 'Hotlinking' in p.html():
                vprint('Hotlinking error; retrying', 1)
            # Restaurant search, but page did not load properly
            elif ('restaurants' in dest_url) and (not p.find('.page-of-pages').text()):
                vprint('No pages found', 2)
            # Business page, cannot check for pages since there might be no reviews
            elif '/biz/' in dest_url:
                time.sleep(wait_time)
                waiting = False
            # Page loaded properly
            else:
                time.sleep(wait_time)
                waiting = False

        except urllib2.HTTPError, e:
            if e.code == 403:
                if no_proxies:
                    vprint('Connection blocked', 1)
                    time.sleep(3*wait_time)
                else:
                    vprint('Connection blocked; switching proxy', 1)
                    proxy = get_proxy()
                    error_count = -1
                    time.sleep(wait_time)
            elif e.code == 404:
                vprint('404 received', 1)
                return None
        except urllib2.URLError:
            vprint('Connection timeout; retrying', 1)
        except Exception:
            vprint('Unknown error received in fetching URL', 1)

        error_count = error_count + 1

    return p



# Yelp attributes:
# Price range
# Takes Reservations
# Delivery
# Take-out
# Accepts Credit Cards
# Good For
# Parking
# Wheelchair Accessible
# Good for Kids
# Good for Groups
# Attire
# Ambience
# Noise Level
# Alcohol
# Outdoor Seating
# Wi-Fi
# Has TV
# Waiter Service
# Drive-Thru
# Caters
# Allows dogs
def get_restaurant_data(node, dest_url, zip_code):
    """Writes to a file the attributes of a given restaurant"""
    if node is None:
        return

    d = PyQuery(node)

    if generic_file_names:
        f = codecs.open(restaurant_data, 'a', 'utf-8')
    else:
        f = codecs.open(str(zip_code).zfill(5)+'_data.csv', 'a', 'utf-8')

    attributes = {'name' : 'undefined', 'addr' : 'undefined', 'id' : 'undefined',
                  'closed' : 'No', 'price' : 'undefined', 'cat' : 'undefined',
                  'delivery' : 'undefined', 'takeout' : 'undefined', 'reser' : 'undefined',
                  'alc' : 'undefined', 'ambience' : 'undefined', 'attire' : 'undefined',
                  'waiter' : 'undefined', 'parking' : 'undefined', 'credit' : 'undefined',
                  'noise' : 'undefined'}

    # Yelp loads two types of pages, requiring different parsers
    if d('body').eq(0).is_('.country-us.logged-out.ytype.borderless.biz-details.jquery'):
        attributes['name'] = d.find('.biz-page-title.embossed-text-white').text()
        attributes['cat'] = d.find('.category-str-list').text()
        attributes['addr'] = d.find('.street-address').text().replace(' , ', ' ')

        # Catches errors if the values were somehow not found
        if (not attributes['name']) or (not attributes['addr']):
            vprint('Information for restaurant not found '+dest_url, 0)
            return

        attributes['id'] = attributes['name'] + attributes['addr']
        attributes['price'] = d.find('.business-attribute.price-range').eq(0).text()

        if d.find('.i-wrap.ig-wrap-biz_details.i-perm-closed-alert-biz_details-wrap').text():
            attributes['closed'] = 'Yes'

        # String replacement operations are to make the formatting consistent
        # with the non-borderless pages
        for i in range(len(d.find('dl'))):
            if 'Delivery' in d.find('dl').eq(i).text():
                attributes['delivery'] = d.find('dl').eq(i).text().replace('Delivery ', 'Delivery: ')
            elif 'Take-out' in d.find('dl').eq(i).text():
                attributes['takeout'] = d.find('dl').eq(i).text().replace('Take-out ', 'Take-out: ')
            elif 'Reservations' in d.find('dl').eq(i).text():
                attributes['reser'] = d.find('dl').eq(i).text().replace('Reservations ', 'Reservations: ')
            elif 'Alcohol' in d.find('dl').eq(i).text():
                attributes['alc'] = d.find('dl').eq(i).text().replace('Alcohol ', 'Alcohol: ')
            elif 'Ambience' in d.find('dl').eq(i).text():
                attributes['ambience'] = d.find('dl').eq(i).text().replace('Ambience ', 'Ambience: ')
            elif 'Attire' in d.find('dl').eq(i).text():
                attributes['attire'] = d.find('dl').eq(i).text().replace('Attire ', 'Attire: ')
            elif 'Waiter' in d.find('dl').eq(i).text():
                attributes['waiter'] = d.find('dl').eq(i).text().replace('Service ', 'Service: ')
            elif 'Parking' in d.find('dl').eq(i).text():
                attributes['parking'] = d.find('dl').eq(i).text().replace('Parking ', 'Parking: ')
            elif 'Credit' in d.find('dl').eq(i).text():
                attributes['credit'] = d.find('dl').eq(i).text().replace('Cards ', 'Cards: ')
            elif 'Noise' in d.find('dl').eq(i).text():
                attributes['noise'] = d.find('dl').eq(i).text().replace('Level ', 'Level: ')

        # Check for presence of data in case an old version of PyQuery returns None
        if attributes['cat']:
            attributes['cat'] = attributes['cat'].replace(' , ', ';')
            # "Beer, Wine & Spirits" category can cause delimiter problems
            attributes['cat'] = attributes['cat'].replace(', ', ' ')
        if attributes['price'] == '' or attributes['price'] is None:
            attributes['price'] = 'undefined'
        if attributes['ambience']:
            attributes['ambience'] = attributes['ambience'].replace(', ', ';')
        if attributes['parking']:
            attributes['parking'] = attributes['parking'].replace(', ', ';')
    else:
        attributes['name'] = d.find('#bizInfoHeader h1').text()
        attributes['price'] = d.find('.price-range.pseudoLink').text()
        attributes['cat'] = d.find('#cat_display a').text()
        attributes['addr'] = d.find('address span').text()

        # Catches errors if the values were somehow not found
        if (not attributes['name']) or (not attributes['addr']):
            vprint('Information for restaurant not found '+dest_url, 0)
            return

        attributes['id'] = attributes['name'] + attributes['addr']
        attributes['delivery'] = d.find('.attr-RestaurantsDelivery').text()
        attributes['takeout'] = d.find('.attr-RestaurantsTakeOut').text()
        attributes['reser'] = d.find('.attr-RestaurantsReservations').text()
        attributes['alc'] = d.find('.attr-Alcohol').text()
        attributes['ambience'] = d.find('.attr-Ambience').text()
        attributes['attire'] = d.find('.attr-RestaurantsAttire').text()
        attributes['waiter'] = d.find('attr-RestaurantsTableService').text()
        attributes['parking'] = d.find('attr-BusinessParking').text()
        attributes['credit'] = d.find('.attr-BusinessAcceptsCreditCards').text()
        attributes['noise'] = d.find('attr-NoiseLevel').text()

        # Check for presence of data in case an old version of PyQuery returns None
        if attributes['cat']:
            # "Beer, Wine & Spirits" category can cause delimiter problems
            attributes['cat'] = attributes['cat'].replace('Beer, ', 'Beer ')
            attributes['cat'] = attributes['cat'].replace(', ', ';')
        if attributes['ambience']:
            attributes['ambience'] = attributes['ambience'].replace(', ', ';')
        if attributes['parking']:
            attributes['parking'] = attributes['parking'].replace(', ', ';')

        # find() returns '' if the item is not found (None in old versions; 
        # supported for compatibility), so if any items were not set on Yelp,
        # return their values to 'undefined'
        for item in attributes:
            if attributes[item] == '' or attributes[item] is None:
                attributes[item] = 'undefined'

    if truncated_data:
        attributes = truncate_data(attributes)

    f.write(attributes['name']+',')
    f.write(attributes['addr']+',')
    f.write(attributes['id']+',')
    f.write(attributes['closed']+',')
    f.write(attributes['price']+',')
    f.write(attributes['cat']+',')
    f.write(attributes['delivery']+',')
    f.write(attributes['takeout']+',')
    f.write(attributes['reser']+',')
    f.write(attributes['alc']+',')
    f.write(attributes['ambience']+',')
    f.write(attributes['attire']+',')
    f.write(attributes['waiter']+',')
    f.write(attributes['parking']+',')
    f.write(attributes['credit']+',')
    f.write(attributes['noise']+'\n')
    f.close()

    get_restaurant_reviews(attributes['name'], attributes['addr'], dest_url, zip_code)

    return



def get_restaurant_reviews(name, addr, original_url, zip_code):
    """Writes to a file the reviews of a given restaurant"""
    d = get_query(original_url)

    if generic_file_names:
        f = codecs.open(restaurant_reviews, 'a', 'utf-8')
    else:
        f = codecs.open(str(zip_code).zfill(5)+'_reviews.csv', 'a', 'utf-8')
        g = codecs.open(str(zip_code).zfill(5)+'_reviews_m.csv', 'a', 'utf-8')

    if d('body').eq(0).is_('.country-us.logged-out.ytype.borderless.biz-details.jquery'):
        borderless = True
        pages = d.find('.page-of-pages').text()

        if pages == '' or pages is None:
            vprint('Number of review pages not found; assuming 1', 1)
            pages = 1
        else:
            pages = int(pages.split(' ')[3])
    else:
        vprint('Number of review pages not found; assuming 1', 1)
        pages = 1

    # Page X of Y
    for page in range(pages):
        vprint('Reviews page '+str(page+1)+' of '+str(pages), 1)
        dest_url = original_url+'&start='+str(page*40)
        vprint(dest_url, 2)
        d = get_query(dest_url)

        if d('body').eq(0).is_('.country-us.logged-out.ytype.borderless.biz-details.jquery'):
            borderless = True
            reviews = d.find('.review.review-with-no-actions')
        else:
            borderless = False
            reviews = d.find('.review')

        if len(reviews) == 0:
            vprint('No reviews', 1)
            return

        for i in range(len(reviews)):
            r = reviews.eq(i)
            attributes = {'reviewer' : 'undefined', 'date' : 'undefined',
                          'stars' : 'undefined', 'text' : 'undefined'}
            if borderless:
                user = r.find('.user-name').find('a').attr('href')
                # Users imported from Qype have no associated Yelp URL
                if user == '' or user is None:
                    attributes['reviewer'] = r.find('.user-name').text()
                else:
                    if no_proxies:
                        attributes['reviewer'] = user
                    else:
                        attributes['reviewer'] = deproxify(user)
                attributes['location'] = r.find('.user-location').text().replace(', ', ' ')
                if american_dates:
                    attributes['date'] = r.find('.rating-qualifier').text() # MM/DD/YYYY
                else:
                    attributes['date'] = r.find('.rating-qualifier meta[itemprop = "datePublished"]')\
                                         .attr('content') # YYYY-MM-DD
                attributes['stars'] = r.find('.rating-very-large meta[itemprop="ratingValue"]')\
                                      .attr('content')
                attributes['text'] = r.find('.review_comment.ieSucks').text()
            else:
                user = r.find('.user-name').find('a').attr('href')
                # Users imported from Qype have no associated Yelp URL
                if user == '' or user is None:
                    attributes['reviewer'] = r.find('.user-name').text()
                else:
                    if no_proxies:
                        attributes['reviewer'] = user
                    else:
                        attributes['reviewer'] = deproxify(user)
                attributes['location'] = find('.reviewer_info').text().replace(', ', ' ')
                if american_dates:
                    attributes['date'] = r.find('.date.smaller').text() # MM/DD/YYYY
                else:
                    attributes['date'] = r.find('.media-story meta[itemprop = "datePublished"]')\
                                         .attr('content') # YYYY-MM-DD
                attributes['stars'] = r.find('.rating meta[itemprop = "ratingValue"]')\
                                      .attr('content')
                attributes['text'] = r.find('.review_comment.ieSucks').text()

            f.write(name+addr+',')
            f.write(attributes['reviewer']+',')
            f.write(attributes['location']+',')
            f.write(attributes['date']+',')
            f.write(attributes['stars']+',')
            f.write('"'+attributes['text'].replace('"', '\"\"')+'"'+'\n')

            g.write(name+',')
            g.write(addr+',')
            g.write(attributes['reviewer']+',')
            g.write(attributes['location']+',')
            g.write(attributes['date']+',')
            g.write(attributes['stars']+',')
            g.write('"'+attributes['text'].replace('"', '\"\"')+'"'+'\n')

    f.close()

    return



def process_zip_code(zip_code):
    """Gets the information and reviews for all restaurants in a zip code"""
    vprint('Processing zip code: '+str(zip_code).zfill(5), 0)

    if not generic_file_names:
        f = codecs.open(str(zip_code).zfill(5)+'_data.csv', 'w', 'utf-8')
        f.write(data_header)
        f.close()
        f = codecs.open(str(zip_code).zfill(5)+'_reviews.csv', 'w', 'utf-8')
        f.write(review_header)
        f.close()
        f = codecs.open(str(zip_code).zfill(5)+'_reviews_m.csv', 'w', 'utf-8')
        f.write(my_review_header)
        f.close()

    # As of 2014/02/22, there are 10 restaurants and 40 reviews to a page
    res_step = 10
    rev_step = 40

    dest_url = 'http://www.yelp.com/search?cflt=restaurants&find_loc='+str(zip_code).zfill(5)+\
               '&start=0&sortby=review_count'

    j = get_query(dest_url)

    # Zip code was invalid or had no restaurants
    if j.find('.clearfix.layout-block.layout-full.search-exception h2').text() or\
       not j.find('.page-of-pages').text():
        return

    # Page X of Y
    pages = j.find('.page-of-pages').text()
    pages = int(pages.split(' ')[3])

    i = 0
    for page in range(pages):
        vprint('Restaurants page '+str(page+1)+' of '+str(pages), 1)
        j = get_query(dest_url)

        restaurant_number = 0
        for x in j('.media-story .biz-name'):
            if no_proxies:
                dest_url = 'http://www.yelp.com'+j(x).attr('href')+'?sort_by=date_desc'
            else:
                dest_url = deproxify(j(x).attr('href'))+'?sort_by=date_desc'

            if ('adredir' not in dest_url):
                if str(zip_code) in j.find('address').eq(restaurant_number).text():
                    vprint(str(zip_code)+':'+str(page)+':'+str(i), 2)
                    vprint(dest_url, 1)
                    p = get_query(dest_url)
                    get_restaurant_data(p, dest_url, zip_code)
                else:
                    vprint('Restaurant not in zip code', 1)
                i += 1
            else:
                vprint('Ad URL skipped', 2)

            restaurant_number += 1

        if page+1 < pages:
            dest_url = 'http://www.yelp.com/search?cflt=restaurants&find_loc='+str(zip_code).zfill(5)+\
                       '&sortby=review_count&start='+str((page+1)*res_step)

    return



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--start', action='store', type=int,
                        default=1001) # Agawa, MA
    parser.add_argument('-e', '--end', action='store', type=int,
                        default=99950) # Ketchikan, AK
    parser.add_argument('-v', '--verbosity', action='store', type=int,
                        default=0)
    parser.add_argument('-p', '--proxy-list', action='store', type=str,
                        default='proxy-list.txt')
    parser.add_argument('-f', '--file', action='store', type=str)
    parser.add_argument('-a', '--american-dates', action='store_true')
    parser.add_argument('-g', '--generic-file-names', action='store_true')
    parser.add_argument('-t', '--truncated_data', action='store_true')
    parser.add_argument('-n', '--no-proxies', action='store_true')
    args = parser.parse_args(sys.argv[1:])

    global verbosity
    verbosity = args.verbosity
    global american_dates
    american_dates = args.american_dates
    global generic_file_names
    generic_file_names = args.generic_file_names
    global truncated_data
    truncated_data = args.truncated_data
    global no_proxies
    no_proxies = args.no_proxies

    # Increase cooldown time to help avoid blocking
    if no_proxies:
        global WAIT_MIN
        WAIT_MIN = WAIT_MIN * 4
        global WAIT_MAX
        WAIT_MAX = WAIT_MAX * 4

    lowest_zip_code = args.start
    highest_zip_code = args.end

    if generic_file_names:
        f = codecs.open(restaurant_data, 'w', 'utf-8')
        f.write(data_header)
        f.close()
        f = codecs.open(restaurant_reviews, 'w', 'utf-8')
        f.write(review_header)
        f.close()

    logging.basicConfig(level=logging.DEBUG)

    # Stores the list of proxy domains in memory
    if not no_proxies:
        proxy_list = open(args.proxy_list, 'r')
        for line in proxy_list:
            proxies.append(line.strip())

        proxy_list.close()

    if args.file:
        zip_code_file = open(args.file, 'r')
        zip_codes = []
        for line in zip_code_file:
            z = line.strip()
            zip_codes.append(int(z))

        for zip_code in zip_codes:
            try:
                process_zip_code(zip_code)
            except Exception as e:
                logging.exception('Error processing zip code {0}\n'.format(zip_code))
    else:
        for zip_code in range(lowest_zip_code, highest_zip_code+1):
            try:
                process_zip_code(zip_code)
            except Exception as e:
                logging.exception('Error processing zip code {0}\n'.format(zip_code))

    sys.exit(0)



if __name__ == "__main__":
    main()

