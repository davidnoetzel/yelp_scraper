#!/usr/bin/python

# Proxy Tester: checks if a free web proxy allows hotlinking
# Copyright (C) 2014  David Noetzel

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import urllib2
from pyquery import PyQuery

def get_query(dest_url, proxy):
    waiting = True

    while waiting:
        try:
            req = urllib2.Request(proxy)
            resp = urllib2.urlopen(req)
            req = urllib2.Request(dest_url)
            req.add_header('Referer', proxy+'/')
            resp = urllib2.urlopen(req)
            p = PyQuery(resp.read())

            if p.find('.page-of-pages').text():
                print 'success '+proxy
                string = g.read()
                if proxy not in string:
                    g.write(proxy+'\n')
            else:
                print 'page not loaded properly '+proxy
        except urllib2.HTTPError:
            print 'error received '+proxy
        except urllib2.URLError:
            print 'timeout '+proxy
        except Exception:
            print 'unknown error '+proxy
        waiting = False

    return

f = open('test-proxies.txt', 'r')
g = open('working-proxies.txt', 'a')
for line in f:
    line = line.strip()
    get_query(line+'/browse.php?u='+'http%3A%2F%2Fwww.yelp.com%2Fbiz%2Fshake-shack-new-york-2%3Fsort_by%3Ddate_desc'+'&b=4', line)

f.close()
g.close()
