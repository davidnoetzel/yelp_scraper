# Yelp Scraper
This is a tool to scrape Yelp for information on American restaurants. The
results were used in Jhamat Mahbubani's (Yale '14) senior economics thesis
looking for a correlation between immigrant populations and quality of ethnic
cuisine. The scraper can either collect information about all the restaurants in
a range of zip codes or in a set of zip codes specified in a file.

Both objective data about the restaurants (cuisine, price, etc.) and reviews for
each restaurant in are saved to CSVs. By default, there are three files created
for each zip code:

1. Objective information for each restaurant in a zip code

2. Reviews for each restaurant in a zip code

3. Same as above, but with a slightly different structure; used in a project for
a databases class

# Proxy Tester
To avoid being blocked by Yelp, the scraper can use free web proxies that
allow hotlikning. To compile a working list of proxies, I used an automated
testing tool to take in a bunch free web proxies and filter out only those
that allowed hotlinking. This tool is included in the `proxy_test` folder, as
are input and output files used to carry out data collection.

# Dependencies
This scraper relies on [PyQuery] to carry out selection operations on the DOM
objects.

[PyQuery]: https://github.com/gawel/pyquery