NAME
    yelp_scraper_new.py - rips information and reviews from Yelp for American
                          restaurants

SYNOPSIS
    python yelp_scraper_new.py [OPTION]


DESCRIPTION
    Scrapes Yelp for information about and reviews for restaurants across the
    USA, from the lowest zip codes to the highest. Recommended configuration:
    python yelp_scraper_new.py -t -v 1 -s ZIP -e ZIP

    -a, --american-dates
            writes dates in the format MM/DD/YYYY instead of the default
            format of YYYY-MM-DD

    -e, --end ZIP
            ends the scrape the specified zip code. The default is 99950

    -f, --filename FILE
            reads in zip codes from a text file, with one zip code per line

    -g, --generic-file-names
            writes all data to files named yelp_restaurant_data.csv and
            yelp_review_data.csv, instead of writing two files, #####_data.csv
            and #####_reviews.csv, per zip code

    -n, --no-proxies
            Do not use proxies when scraping

    -s, --start ZIP
            starts the scrape the specified zip code. The default is 01001

    -t, --truncated_data
            remove the title header from entries. E.g., "Yes" instead of
            "Accepts Credit Cards: Yes"

    -v, --verbosity LEVEL
            prints extra messages; used levels are 0, 1 and 2

    The generated CSVs are separated with commas. Therefore, to prevent read
    issues, data that would normally have commas (cuisine categories, ambience,
    parking) instead have semicolons so that the data can be split easily in
    further analysis. E.g. "American (New), Vegetarian" becomes
    "American (New);Vegetarian". Commas were simply removed from street
    addresses. The problematic "Beer, Wine & Spirits" category is renamed to
    "Beer Wine & Spirits" to prevent delimiter issues. Review text is kept
    intact; to prevent delimiter issues, quotes are replaced with two pairs of
    quotation marks. Column labels are written to the first line of every file.
